<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="doc"/>
	<xsl:template match="/">
		<!-- TODO: Auto-generated template -->
		
		<html>
		<head>
			<style>
				table, th, td{
					border: 1px solid black;
					border-collapse: collapse;
				}
			</style>
			
		</head>
			<body>
				<h2>Mitarbeiter</h2>
			
				<form>
				<label>Mitarbeiter ID:
				
				<select>
					
					<xsl:for-each select="Datenbank/Mitarbeiter">
						
						<option><xsl:value-of select="ID"/></option>
						
					</xsl:for-each>	
				
				</select>
				
				</label>
				</form>
				<h2>Mitarbeiter</h2>
				<table>
				<tr>
					<th>ID</th>
					<th>Vorname</th>
					<th>Nachname</th>
				</tr>
				
				<xsl:for-each select="Datenbank/Mitarbeiter">
				<tr>
				
					
					
					
					
				<td>
					<xsl:value-of select="ID"/>
					
				</td>
					
				<td>
					
					
					<xsl:value-of select="Name/Vorname"/>
					
					
				</td>
					
				<td>
					
					<xsl:value-of select="Name/Nachname"/>
					
				</td>
					
				
				</tr>
				</xsl:for-each>
				</table>
				
			</body>
		</html>
		
	</xsl:template>
</xsl:stylesheet>
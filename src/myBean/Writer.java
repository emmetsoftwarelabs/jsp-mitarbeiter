package myBean;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class Writer {
	
	

	private final static String DATNAM = "C:\\Users\\Florian\\eclipse-workspace\\JSP-Mitarbeiter\\src\\Datenbank.xml";
	private final static File FILE = new File(DATNAM);
	private Document doc;
	
	
	public Writer() {
		doc = new Document();
		doc = this.createDoc("Datenbank");
	  
	}
	 
	
	private Document createDoc(String rootElement) {
		Element root = new Element(rootElement);
		doc.setRootElement(root);
		return doc;
	}
	
	public void writeDoc(Element e) {
        doc.getRootElement().addContent(e);
	}
	
	public Element createMitarbeiter(String vName, String nName ,long id) {
		
		Element mitarbeiter = new Element("Mitarbeiter");
		
		mitarbeiter.setAttribute("id", Long.toString(id));
		Element name = new Element("Name");
        
        Element vorname = new Element("Vorname");
        Element nachname = new Element("Nachname");
        
        vorname.setText(vName);
        nachname.setText(nName);
        
        name.addContent(vorname);
        name.addContent(nachname);
        
        mitarbeiter.addContent(name);
		
        return mitarbeiter;
	}
	
	public void writeXML(Document doc) {
        Format format = Format.getPrettyFormat();
        format.setIndent("    ");
        try (FileOutputStream fos = new FileOutputStream(FILE)) {
            XMLOutputter op = new XMLOutputter(format);
            op.output(doc, fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public Document getDoc() {
		return doc;
	}
	
	public static void main(String[] args) {
		Writer writer = new Writer();
		writer.writeXML(writer.getDoc());
	}
}

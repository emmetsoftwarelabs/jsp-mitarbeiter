package myBean;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

@WebServlet("/search")
public class XMLReader extends HttpServlet{
	
	private final static String DATNAME = "C:\\Users\\Florian\\eclipse-workspace\\JSP-Mitarbeiter\\src\\Datenbank.xml";
	private String nachname = "";
	private String vorname = "";
	private String id = "";
	private Element root;
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String id = request.getParameter("suche");
		
		setId(id);
		setNamen();
		PrintWriter writer = response.getWriter();
		
		String htmlResponde = "<html>";
		htmlResponde += "<head>\r\n"
				+ "\r\n"
				+ "<style>\r\n"
				+ "	table{\r\n"
				+ "		font-family: arial, sans-serif;\r\n"
				+ "		border-collapse: collapse;\r\n"
				+ "		width: 100%;\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	td, th {\r\n"
				+ "		border: 1px solid #dddddd;\r\n"
				+ "		text-algin: left;\r\n"
				+ "		padding: 8px;\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	tr:nth-child(even){\r\n"
				+ "		backround-color: #dddddd\r\n"
				+ "	}\r\n"
				+ "</style>\r\n"
				+ "\r\n"
				+ "<meta charset=\"ISO-8859-1\">\r\n"
				+ "<title>Mitarbeiterliste</title>\r\n"
				+ "</head>";
		
		
		htmlResponde += "<body>\r\n"
				+ "\r\n"
				+ "	\r\n"
				+ "		<table>\r\n"
				+ "			<tr>\r\n"
				+ "				<th>ID</th>\r\n"
				+ "				<th>Vorname</th>\r\n"
				+ "				<th>Nachname</th>\r\n"
				+ "			</tr>\r\n"
				+ "			<tr>\r\n"
				+ "			\r\n"
				+ "				<td>" + this.id + "</td>\r\n"
				+ "				<td>" +this.vorname+ "</td>\r\n"
				+ "				<td>" +this.nachname+ "</td>\r\n"
				+ "			</tr>\r\n"
				+ "		</table>\r\n"
				+ "	\r\n"
				+ "\r\n"
				+ "</body>";
		htmlResponde += "</html>";
		writer.println(htmlResponde);
	}
	
	private void readXML() {
		try {
			
			Document doc = new SAXBuilder().build(DATNAME);
			root = doc.getRootElement();
			
			} catch(IOException e) {
				e.printStackTrace();
			} catch(JDOMException e) {
				e.printStackTrace();
			}
	}
	
	
	
	private void setName(Element e, String nachVorName) {
		
		if(e.getName().equals("Mitarbeiter")) {
			
			if(e.getAttribute("id").getValue().equals(id)) {
				
				if(nachVorName.equals("NACHNAME")) {
					nachname = e.getChild("Name").getChild("Nachname").getValue();
				}else if(nachVorName.equals("VORNAME")) {
					vorname = e.getChild("Name").getChild("Vorname").getValue();
				}else {
					System.out.println("ERROR: Flasche angabe nur NACHNAME oder VORNAME sind m�glich");
				}
			}else {
				getChilds(e.getChildren(), nachVorName);
			}
		}else if(!(e.getName().equals("Mitarbeiter"))) {
			getChilds(e.getChildren(),nachVorName);
		}
		
	}
	
	
	public void setNamen() {
		readXML();
		nachname = "";
		vorname = "";
		
		setNachname();
		setVorname();
	}
	
	public void setNachname() {
		setName(root,"NACHNAME");
		
	}
	
	public void setVorname() {
		setName(root,"VORNAME");
	}
	
	public String getNachname() {
		
		return nachname;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}
	
	private void getChilds(List<Element> list, String nachVorName) {
		for(Element e : list) {
			setName(e,nachVorName);
		}
		
	}
	
	public static void main(String[] args) {
        XMLReader xmlReader = new XMLReader();
        xmlReader.setId("1");
        xmlReader.setVorname();
        xmlReader.setNachname();
        
        System.out.println(xmlReader.getVorname());
        System.out.println(xmlReader.getNachname());
	}
}

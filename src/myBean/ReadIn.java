package myBean;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/add")
public class ReadIn extends HttpServlet {

	private long id = 1;
	private Writer w = new Writer();
	private String vorname="";
	private String nachname="";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String vorname = request.getParameter("vorname");
		String nachname = request.getParameter("nachname");
		
		setVorname(vorname);
		setNachname(nachname);
		addMitarbeiter();
		
		PrintWriter writer = response.getWriter();
		
		String htmlResponde = "<html>";
		htmlResponde += "<h2>Erfolgreich hinzugefügt</h2>";
		htmlResponde += "<button><a href=\"" + "http://localhost:8080/JSP-Mitarbeiter/Mitarbeiter.jsp" + "\"/> Weitere hinzufügen</button>";
		htmlResponde += "</html>";
		
		writer.println(htmlResponde);
	}
	
	public void addMitarbeiter() {
			if(!(vorname.trim().equals("")) && !(nachname.trim().equals(""))) {
				w.writeDoc(w.createMitarbeiter(vorname,nachname,id));
				w.writeXML(w.getDoc());
				id += 1;
			}
		
	}



	public String getVorname() {
		return vorname;
	}



	public void setVorname(String vorname) {
		this.vorname = vorname;
		
	}



	public String getNachname() {
		return nachname;
	}



	public void setNachname(String nachname) {
		this.nachname = nachname;
		
	}
	
	
}

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="doc"/>
	<xsl:template match="/">
		<!-- TODO: Auto-generated template -->
		<html>
			<body>
				<h2>Mitarbeiter</h2>
			
				<form>
				<label>Mitarbeiter ID:
				
				<select>
					
					<xsl:for-each select="Datenbank/Mitarbeiter">
						
						<option><xsl:value-of select="ID"/></option>
						
					</xsl:for-each>	
				
				</select>
				
				</label>
				</form>
			</body>
		</html>
		
	</xsl:template>
</xsl:stylesheet>